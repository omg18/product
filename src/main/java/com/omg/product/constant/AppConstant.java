package com.omg.product.constant;

/**
 * @author HieuTT
 * This class defines all constant.
 */
public final class AppConstant {

    private AppConstant() {
    }

    public static String _COLON = ": ";
    public static String _SPACE = " ";

    public static final String DEFAULT_PAGE_NUMBER = "0";
    public static final String DEFAULT_PAGE_SIZE = "30";
    public static final Integer MAX_PAGE_SIZE = 30;

    /** Datetime constant */
    public static final String LOCAL_DATETIME_DEFAULT_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    /**
     * Define the labels which are using in Application.
     * It is better when using the i18n.
     */
    public static class _LABEL {
        public static String _ERROR_MESSAGE = "Error message";
    }

    public static class RESPONSE_TYPE {
        public static final String SUCCESS = "SUCCESS";
        public static final String ERROR = "ERROR";
        public static final String WARNING = "WARNING";
        public static final String CONFIRM = "CONFIRM";

        private RESPONSE_TYPE() {
        }
    }

    public static class RESPONSE_CODE {
        public static final String SUCCESS = "success";
        public static final String DELETE_SUCCESS = "deleteSuccess";
        public static final String ERROR = "error";
        public static final String WARNING = "warning";
        public static final String RECORD_DELETED = "record.deleted";
        public static final String DUPLICATED = "record.duplicated";
        public static final String NOT_FOUND = "record.notFound";

        private RESPONSE_CODE() {
        }
    }

    public static class ACTIVE {
        public static final Integer IS_ACTIVE = 0;
        public static final Integer NOT_ACTIVE = 1;

        private ACTIVE() {
        }
    }

}
