package com.omg.product.constant;

/**
 * @author hieudn
 * This class defines all enum of UUID prefix.
 */
public enum UUIDPrefixEnum {
    UNIT_PRICE("UNIT_PRICE_");
    private final String value;

    UUIDPrefixEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
