package com.omg.product.controller;

import com.omg.product.exception.ProductBusinessException;
import com.omg.product.model.request.CategoryRequest;
import com.omg.product.model.response.CategoryResponse;
import com.omg.product.service.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author hieutt
 * Category Controller
 */
@RestController
@RequestMapping("api/category")
public class CategoryController {

    @Autowired
    private ICategoryService categoryService;

    /**
     * Create category
     *
     * @param request CategoryRequest
     */
    @PostMapping(path = "/create")
    public void create(@RequestBody @Valid CategoryRequest request) throws ProductBusinessException {
        this.categoryService.createCategory(request);
    }

    /**
     * Update category
     *
     * @param request CategoryRequest
     * @return ResponseUtils
     */
    @PostMapping(path = "/update")
    public CategoryResponse update(@RequestBody @Valid CategoryRequest request) throws ProductBusinessException {
        return this.categoryService.updateCategoryByCode(request);
    }
}
