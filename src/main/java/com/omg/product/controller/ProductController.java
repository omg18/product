package com.omg.product.controller;

import com.omg.product.exception.ProductBusinessException;
import com.omg.product.model.bo.ProductDetailBO;
import com.omg.product.model.request.ProductRequest;
import com.omg.product.model.request.ProductPageRequest;
import com.omg.product.model.response.PageResultResponse;
import com.omg.product.model.response.ProductResponse;
import com.omg.product.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author hieudn
 * Product Controller
 */
@RestController
@RequestMapping("api/product")
public class ProductController {

    @Autowired
    private IProductService productService;

    /**
     * Create product
     *
     * @param request ProductRequest
     */
    @PostMapping(path = "/create")
    public void create(@RequestBody @Valid ProductRequest request) throws ProductBusinessException {
        this.productService.createProduct(request);
    }

    /**
     * Update product
     *
     * @param request ProductRequest
     */
    @PutMapping(path = "/update")
    public ProductResponse update(@RequestBody @Valid ProductRequest request) throws ProductBusinessException {
        return this.productService.updateProduct(request);
    }

    /**
     * Get product page
     *
     * @param request
     * @return
     */
    @GetMapping(path = "/page/by-category")
    public PageResultResponse<ProductDetailBO> getPageProductsByCategory(@RequestParam @NotNull String categoryCode)
            throws ProductBusinessException {
        return this.productService.getProductPagesByCategory(categoryCode);
    }

    @GetMapping(path = "/page/by-codes")
    public PageResultResponse<ProductDetailBO> getPageProductsByCodes(
            @RequestParam Integer pageIndex,
            @RequestParam Integer pageSize,
            @RequestParam List<String> productCodes)
            throws ProductBusinessException {
        return this.productService.getProductPagesByCodes(pageIndex, pageSize, productCodes);
    }

    @GetMapping(path = "/by-codes")
    public List<ProductDetailBO> getProductsByCodes(@RequestParam List<String> productCodes) throws ProductBusinessException {
       return this.productService.getProductByCodes(productCodes);
    }

    /**
     * Get product detail by product's code
     *
     * @param code
     * @return
     */
    @GetMapping("/details/{code}")
    public ProductDetailBO getProductDetail(@PathVariable("code") @NotNull String code) throws ProductBusinessException {
        return this.productService.getProductDetail(code);
    }

    /**
     * Delete a product by code
     *
     * @param code
     */
    @PutMapping("/delete/{code}")
    public void deleteProduct(@PathVariable("code") @NotNull String code) throws ProductBusinessException {
        this.productService.deleteProductByCode(code);
    }
}
