package com.omg.product.controller;

import com.omg.product.exception.ProductBusinessException;
import com.omg.product.model.request.VendorRequest;
import com.omg.product.model.response.VendorResponse;
import com.omg.product.service.IVendorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author luongtt
 *
 * Provide the APIs to interact with vendor info.
 */
@RestController
@RequestMapping("api/vendor")
@Slf4j
public class VendorController {

    @Autowired
    private IVendorService vendorService;

    @PostMapping("/create")
    public void createVendor(@RequestBody @Valid VendorRequest vendorRequest) throws ProductBusinessException {
        this.vendorService.createVendor(vendorRequest);
    }

    @GetMapping("/{code}")
    public VendorResponse getVendorByCode(@PathVariable("code") @NotNull String code) throws ProductBusinessException {
        return this.vendorService.getVendorByCode(code);
    }

    @PutMapping("/update")
    public VendorResponse updateVendorByCode(@RequestBody @Valid VendorRequest vendorRequest) throws ProductBusinessException {
        return this.vendorService.updateVendorByCode(vendorRequest);
    }

    @PutMapping("/delete/{code}")
    public void deleteVendorByCode(@PathVariable("code") @NotNull String code) {
        this.vendorService.deleteVendorByCode(code);
    }
}
