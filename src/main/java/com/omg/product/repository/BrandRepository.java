package com.omg.product.repository;

import com.omg.product.entity.Brand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author hieudn
 */
@Repository
public interface BrandRepository extends JpaRepository<Brand, Long> {

}
