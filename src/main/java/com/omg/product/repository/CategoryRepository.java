package com.omg.product.repository;

import com.omg.product.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author luongtt
 */
@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    int countAllByCode(String code);

    Optional<Category> findByCode(String code);

    @Query(value = "SELECT * FROM category c WHERE c.id = :id AND  c.is_deleted != 1", nativeQuery = true)
    Optional<Category> findById(@Param("id") long id);
}
