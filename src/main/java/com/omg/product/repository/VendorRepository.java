package com.omg.product.repository;

import com.omg.product.entity.Vendor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author luongtt
 */
@Repository
public interface VendorRepository extends JpaRepository<Vendor, Long> {

    int countAllByCode(String code);

    @Query(value = "SELECT * FROM vendor v WHERE v.code = :code AND  v.is_deleted != 1", nativeQuery = true)
    Optional<Vendor> findByCode(@Param("code") String code);

    @Modifying
    @Query(value = "UPDATE vendor v SET v.is_deleted = 1 WHERE v.code = :code", nativeQuery = true)
    void disableVendor(@Param("code") String code);

    @Query(value = "SELECT * FROM vendor v WHERE v.id = :id AND  v.is_deleted != 1", nativeQuery = true)
    Optional<Vendor> findById(@Param("id") long id);
}
