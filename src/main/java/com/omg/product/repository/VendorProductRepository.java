package com.omg.product.repository;

import com.omg.product.entity.VendorProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author luongtt
 */
@Repository
public interface VendorProductRepository extends JpaRepository<VendorProduct, Long> {

    @Query(value = "UPDATE vendor_product vp SET vp.isDeleted = 1 WHERE vp.vendor_id = ?1", nativeQuery = true)
    void disableVendorProduct(Long vendorId);

    @Query(value = "SELECT * FROM vendor_product vp WHERE vp.product_id = :product_id AND vp.is_deleted != 1", nativeQuery = true)
    Optional<VendorProduct> findByProductId(@Param("product_id") Long productId);

    @Query(value = "SELECT * FROM vendor_product vp WHERE vp.vendor_id = :vendor_id AND vp.product_id = :product_id AND vp.is_deleted != 1", nativeQuery = true)
    Optional<VendorProduct> findByVendorId(@Param("vendor_id") Long vendorId, @Param("product_id") Long productId);

    List<Long> getIdsVendorProductIdByVendorId(Long vendorId);
}
