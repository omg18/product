package com.omg.product.repository;

import com.omg.product.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author luongtt
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    @Query(value = "UPDATE product p SET p.isDeleted = 1 WHERE p.id in (?1)", nativeQuery = true)
    void disableProductByVendorIds(List<Long> vendorIds);

    @Query(value = "SELECT * FROM product p WHERE p.code = :code AND  p.is_deleted != 1", nativeQuery = true)
    Optional<Product> findByCode(@Param("code") String code);

    int countAllByCode(String code);
}
