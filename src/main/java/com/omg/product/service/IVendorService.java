package com.omg.product.service;

import com.omg.product.exception.ProductBusinessException;
import com.omg.product.model.request.VendorRequest;
import com.omg.product.model.response.VendorResponse;

/**
 * @author luongtt
 */
public interface IVendorService {
    void createVendor(VendorRequest vendorRequest) throws ProductBusinessException;

    VendorResponse getVendorByCode(String code) throws ProductBusinessException;

    VendorResponse updateVendorByCode(VendorRequest vendorRequest) throws ProductBusinessException;

    void deleteVendorByCode(String code);
}
