package com.omg.product.service;

import com.omg.product.exception.ProductBusinessException;
import com.omg.product.model.request.CategoryRequest;
import com.omg.product.model.response.CategoryResponse;

/**
 * @author hieutt
 * ICategoryService
 */
public interface ICategoryService {

    CategoryResponse findCategoryByCode(String code) throws ProductBusinessException;

    void createCategory(CategoryRequest request) throws ProductBusinessException;

    CategoryResponse updateCategoryByCode(CategoryRequest request) throws ProductBusinessException;
}
