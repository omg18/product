package com.omg.product.service;

import com.omg.product.exception.ProductBusinessException;
import com.omg.product.model.bo.ProductDetailBO;
import com.omg.product.model.request.ProductRequest;
import com.omg.product.model.request.ProductPageRequest;
import com.omg.product.model.response.PageResultResponse;
import com.omg.product.model.response.ProductResponse;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author hieudn
 */
public interface IProductService {

    void createProduct(ProductRequest productRequest) throws ProductBusinessException;

    ProductResponse updateProduct(ProductRequest productRequest) throws ProductBusinessException;

    PageResultResponse<ProductDetailBO> getProductPagesByCategory(String categoryCode) throws ProductBusinessException;

    ProductDetailBO getProductDetail(String code) throws ProductBusinessException;

    void deleteProductByCode(String code) throws ProductBusinessException;

    PageResultResponse<ProductDetailBO> getProductPagesByCodes(Integer pageIndex, Integer pageSize, List<String> productCodes) throws ProductBusinessException;

    List<ProductDetailBO> getProductByCodes(List<String> productCodes) throws ProductBusinessException;
}
