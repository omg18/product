package com.omg.product.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.omg.product.entity.Product;
import com.omg.product.entity.VendorProduct;
import com.omg.product.exception.ProductBusinessException;
import com.omg.product.mapper.IProductMapper;
import com.omg.product.model.bo.ProductDetailBO;
import com.omg.product.model.bo.VendorBO;
import com.omg.product.model.request.ProductRequest;
import com.omg.product.model.request.ProductPageRequest;
import com.omg.product.model.response.ProductResponse;
import com.omg.product.model.response.PageResultResponse;
import com.omg.product.repository.CategoryRepository;
import com.omg.product.repository.ProductRepository;
import com.omg.product.repository.VendorProductRepository;
import com.omg.product.repository.VendorRepository;
import com.omg.product.service.IProductService;
import com.omg.product.utils.AppUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * @author hieudn
 * ProductServiceImpl
 */
@Service
@Slf4j
public class ProductServiceImpl implements IProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private VendorRepository vendorRepository;

    @Autowired
    private VendorProductRepository vendorProductRepository;

    @Autowired
    private IProductMapper productMapper;

    /**
     * Create an product
     *
     * @param request
     * @throws ProductBusinessException
     */
    @Override
    @Transactional
    public void createProduct(ProductRequest request) throws ProductBusinessException {

        // Check exist the product with the code in the database
        if (this.productRepository.countAllByCode(request.getCode()) != 0) {
            throw new ProductBusinessException("duplicate product code: " + request.getCode());
        }

        // Check if not exist categoryId and vendorId
        if (this.categoryRepository.findById(request.getCategoryId()).isEmpty()) {
            throw new ProductBusinessException("cannot find categoryId " + request.getCategoryId());
        }

        if (this.vendorRepository.findById(request.getVendorId()).isEmpty()) {
            throw new ProductBusinessException("cannot find vendorId " + request.getVendorId());
        }

        // Prepare data for create new product
        Product product = Product.builder().code(request.getCode())
                .name(request.getName())
                .info(request.getInfo())
                .categoryId(request.getCategoryId())
                .brandId(request.getBrandId())
                .isDeleted(false).build();

        Product productSaveResult = this.productRepository.save(product);

        // Prepare data for create new vendor product
        VendorProduct vendorProduct = VendorProduct.builder().vendorId(request.getVendorId())
                .productId(productSaveResult.getId())
                .isDeleted(false).build();

        this.vendorProductRepository.save(vendorProduct);
    }

    /**
     * Update the info of product
     * @param productRequest
     * @return
     * @throws ProductBusinessException
     */
    @Override
    public ProductResponse updateProduct(ProductRequest productRequest) throws ProductBusinessException {

        //check exits the code product in DB
        String productCode = productRequest.getCode();
        Optional<Product> productEntity = productRepository.findByCode(productCode);
        if (productEntity.isEmpty()) {
            throw new ProductBusinessException("no data for code: " + productCode);
        }
        Product productData = productEntity.get();

        //check exits the vendor product in DB
        Optional<VendorProduct> vendorProductEntity =
                vendorProductRepository.findByVendorId(productRequest.getVendorId(),productData.getId());
        if (vendorProductEntity.isEmpty()) {
            throw new ProductBusinessException("no data for vendor: " + productRequest.getVendorId());
        }

        // Prepare data for update product info
        productData.setName(!StringUtils.isEmpty(productRequest.getName())? productRequest.getName().trim():productData.getName());
        productData.setInfo(!StringUtils.isEmpty(productRequest.getInfo())? productRequest.getInfo().trim():productData.getInfo());
        productData.setCategoryId(productRequest.getCategoryId()!=0 ? productRequest.getCategoryId():productData.getCategoryId());

        //update product info in DB
        this.productRepository.save(productData);

        // Get the updated product info and return
        ProductResponse productResponse = new ProductResponse();
        AppUtils.convertObject(productData, productResponse);
        return productResponse;
    }

    /**
     * Get product pages by category
     *
     * @param request
     * @return
     */
    @Override
    public PageResultResponse<ProductDetailBO> getProductPagesByCategory(String categoryCode) throws ProductBusinessException {

        // Prepare parameter to get data from database
        ProductPageRequest request  = new ProductPageRequest();
        request.setCategoryCode(categoryCode);

        // Check category code
        if (request.getCategoryCode() != null && request.getCategoryCode().isEmpty()) {
            throw new ProductBusinessException("The category code is not empty");
        }

        return this.getProductPages(request);
    }

    /**
     * Get product pages by category
     *
     * @param pageIndex, pageSize, productCodes
     * @return
     */
    @Override
    public PageResultResponse<ProductDetailBO> getProductPagesByCodes(
            Integer pageIndex, Integer pageSize, List<String> productCodes) throws ProductBusinessException {

        // Check the list product codes is empty.
        if (CollectionUtils.isEmpty(productCodes)) {
            throw new ProductBusinessException("The product codes list is not empty");
        }
        return this.getProductPages(
                ProductPageRequest
                        .builder()
                        .pageIndex(pageIndex)
                        .pageSize(pageSize)
                        .productCodes(productCodes)
                        .build());
    }

    @Override
    public List<ProductDetailBO> getProductByCodes(List<String> productCodes) throws ProductBusinessException {

        // Check the list product codes is empty.
        if (CollectionUtils.isEmpty(productCodes)) {
            throw new ProductBusinessException("The product codes list is not empty");
        }

        // Get product info
        ProductPageRequest requests = new ProductPageRequest();
        requests.setProductCodes(productCodes);
        return  this.productMapper.queryProductByCodes(requests);
    }

    private PageResultResponse<ProductDetailBO> getProductPages(ProductPageRequest requests) {

        // Check page size and page index and set default value.
        Integer pageIndex = requests.getPageIndex();
        Integer pageSize = requests.getPageSize();
        if (Optional.ofNullable(pageIndex).orElse(0) == 0
                || Optional.ofNullable(pageSize).orElse(0) == 0) {
            pageIndex = 1;
            pageSize = 10;
        }

        // Get list product detail info
        PageHelper.startPage(pageIndex, pageSize);
        List<ProductDetailBO> productDetails  =  this.productMapper.queryProductByCodes(requests);
        PageInfo<ProductDetailBO> pageInfo = new PageInfo<>(productDetails);

        // Prepare data for response
        PageResultResponse<ProductDetailBO> resultResponse = new PageResultResponse<ProductDetailBO>();
        resultResponse.setContent(productDetails);
        resultResponse.setPageIndex(pageIndex);
        resultResponse.setPageSize(pageSize);
        resultResponse.setTotalPages(pageInfo.getPages());
        resultResponse.setTotalElements(pageInfo.getTotal());

        return resultResponse;
    }
    /**
     * Get product details
     *
     * @param code
     * @return
     */
    @Override
    public ProductDetailBO getProductDetail(String code) throws ProductBusinessException {

        // Get the product detail info
        ProductDetailBO productDetailBO = this.productMapper.queryProductDetail(code);

        // Get the vendor info
        Optional<VendorBO> vendor = this.productMapper.getVendorInfoOfProduct(productDetailBO.getId());
        if (vendor.isEmpty()) {
            throw new ProductBusinessException("There is not any vendor provide the product with code: " + code);
        }
        VendorBO vendorData = vendor.get();
        productDetailBO.setVendorCode(vendorData.getVendorCode());
        productDetailBO.setVendorName(vendorData.getVendorName());

        return productDetailBO;
    }

    /**
     * Delete a product by code
     *
     * @param code
     * @throws ProductBusinessException
     */
    @Override
    @Transactional
    public void deleteProductByCode(String code) throws ProductBusinessException {

        // Get product from database
        Optional<Product> product= this.productRepository.findByCode(code);

        // Check product exist or not in database.
        if(product.isEmpty()) {
            throw new ProductBusinessException("no data for code: " + code);
        }

        // Execute delete the product (just set value of the deleted flag is true)
        Product productData = product.get();
        productData.setDeleted(true);
        this.productRepository.save(productData);

        // Delete the vendorProduct record.
        Long productId = productData.getId();
        Optional<VendorProduct>  vendorProduct = this.vendorProductRepository.findByProductId(productId);
        if(vendorProduct.isEmpty()) {
            return;
        }

        // Execute deleting
        VendorProduct vendorProductData = vendorProduct.get();
        vendorProductData.setDeleted(true);
        this.vendorProductRepository.save(vendorProductData);
    }
}
