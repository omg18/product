package com.omg.product.service.impl;

import com.omg.product.entity.Vendor;
import com.omg.product.exception.ProductBusinessException;
import com.omg.product.mapper.IVendorMapper;
import com.omg.product.model.request.VendorRequest;
import com.omg.product.model.response.VendorResponse;
import com.omg.product.repository.CategoryRepository;
import com.omg.product.repository.ProductRepository;
import com.omg.product.repository.VendorProductRepository;
import com.omg.product.repository.VendorRepository;
import com.omg.product.service.IVendorService;
import com.omg.product.utils.AppUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * @author luongtt
 *
 * Process the business of the vendor
 */
@Service
@Slf4j
@CacheConfig(cacheNames = "vendorCache")
public class VendorServiceImpl implements IVendorService {

    @Autowired
    private VendorRepository vendorRepository;

    @Autowired
    private VendorProductRepository vendorProductRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private IVendorMapper vendorMapper;

    /**
     * Create new a vendor
     * @param vendorRequest
     * @throws ProductBusinessException
     */
    @Override
    @Transactional
    public void createVendor(VendorRequest vendorRequest) throws ProductBusinessException {

        // Check exist the vendor with the code in the database
        if (this.vendorRepository.countAllByCode(vendorRequest.getCode()) != 0) {
            throw new ProductBusinessException("duplicate code: " + vendorRequest.getCode());
        }

        // Update the info of vendor
        Vendor vendor = Vendor.builder().code(vendorRequest.getCode())
                .name(vendorRequest.getName())
                .description(vendorRequest.getDescription())
                .phone(vendorRequest.getPhone())
                .address(vendorRequest.getAddress()).build();
        this.vendorRepository.save(vendor);
    }

    /**
     * Get the vendor info by vendor code
     * @param code
     * @return
     * @throws ProductBusinessException
     */
    @Override
    @Cacheable(cacheNames = "vendor", key = "#code", unless = "#result == null")
    public VendorResponse getVendorByCode(String code) throws ProductBusinessException {

        // Check exist the vendor with the code in the database
        Optional<Vendor> vendorEntity = vendorMapper.findVendorByCode(code);
        if (vendorEntity.isEmpty()) {
            throw new ProductBusinessException("no data for code: " + code);
        }

        return convertVendorResponse(vendorEntity.get());
    }

    /**
     * Update the vendor info by vendor code
     *
     * @param vendorRequest
     * @return VendorResponse
     * @throws ProductBusinessException
     */
    @Override
    @Transactional
    @CachePut(cacheNames="vendor", key = "#vendorRequest.code")
    public VendorResponse updateVendorByCode(VendorRequest vendorRequest) throws ProductBusinessException {

        // Check exist the vendor in the database
        String vendorCode = vendorRequest.getCode();
        Optional<Vendor> vendorEntity = vendorRepository.findByCode(vendorCode);
        if (vendorEntity.isEmpty()) {
            throw new ProductBusinessException("no data for code: " + vendorCode);
        }

        // Prepare data for update vendor info
        Vendor vendorData = vendorEntity.get();
        vendorData.setName(!StringUtils.isEmpty(vendorRequest.getName()) ? vendorRequest.getName() : vendorData.getName());
        vendorData.setAddress(!StringUtils.isEmpty(vendorRequest.getAddress()) ? vendorRequest.getAddress() : vendorData.getAddress());
        vendorData.setDescription(!StringUtils.isEmpty(vendorRequest.getDescription()) ? vendorRequest.getDescription() : vendorData.getDescription());
        vendorData.setPhone(!StringUtils.isEmpty(vendorRequest.getPhone()) ? vendorRequest.getPhone() : vendorData.getPhone());

        // Execute update
        return this.convertVendorResponse(vendorRepository.save(vendorData));
    }

    private VendorResponse convertVendorResponse(Vendor vendor) {
        return VendorResponse.builder()
                .id(vendor.getId())
                .code(vendor.getCode())
                .address(vendor.getAddress())
                .name(vendor.getName())
                .phone(vendor.getPhone())
                .description(vendor.getDescription())
                .isDeleted(vendor.isDeleted())
                .createdTime(vendor.getCreatedTime())
                .createdBy(vendor.getCreatedBy())
                .updatedTime(vendor.getUpdatedTime())
                .updatedBy(vendor.getUpdatedBy()).build();
    }

    /**
     * Delete the vendor info by vendor code
     * @param code
     */
    @Override
    @Transactional
    @CacheEvict(cacheNames="vendor", key = "#code")
    public void deleteVendorByCode(String code) {

        // Delete the vendor
        // Check exist the vendor in database
        Optional<Vendor> vendorEntity = vendorRepository.findByCode(code);
        if (!vendorEntity.isPresent()) {
            return;
        }

        // Execute the delete vendor (just disable logic)
        vendorRepository.disableVendor(code);

        // Get vendor id
        Long vendorId = vendorEntity.get().getId();

        // Delete the vendor product, product
        List<Long> lstVendorProductId = vendorProductRepository.getIdsVendorProductIdByVendorId(vendorId);
        if (!lstVendorProductId.isEmpty()) {
            productRepository.disableProductByVendorIds(lstVendorProductId);
            vendorProductRepository.disableVendorProduct(vendorId);
        }
    }
}
