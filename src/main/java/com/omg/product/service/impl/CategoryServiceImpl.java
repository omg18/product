package com.omg.product.service.impl;

import com.omg.product.utils.AppUtils;
import com.omg.product.entity.Category;
import com.omg.product.exception.ProductBusinessException;
import com.omg.product.model.request.CategoryRequest;
import com.omg.product.model.response.CategoryResponse;
import com.omg.product.repository.CategoryRepository;
import com.omg.product.service.ICategoryService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author hieutt
 * CategoryServiceImpl
 */
@Service
@Slf4j
public class CategoryServiceImpl implements ICategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    /**
     * Get the category info by code
     * @param code
     * @return
     * @throws ProductBusinessException
     */
    @Override
    public CategoryResponse findCategoryByCode(String code) throws ProductBusinessException {

        // Get the category info from database
        Optional<Category> optional = categoryRepository.findByCode(code);

        // Check exist the category in the database
        if (optional.isEmpty()) {
            throw new ProductBusinessException("no data for code: " + code);
        }

        // Get the data for response body
        CategoryResponse response = new CategoryResponse();
        AppUtils.convertObject(optional.get(), response);
        return response;
    }

    /**
     *
     * @param request
     * @throws ProductBusinessException
     */
    @Override
    public void createCategory(CategoryRequest request) throws ProductBusinessException {

        // Check exist the category with the code in the database
        if (this.categoryRepository.countAllByCode(request.getCode()) != 0) {
            throw new ProductBusinessException("duplicate code: " + request.getCode());
        }

        // Prepare data for create new category
        Category category = new Category();
        AppUtils.convertObject(request, category);

        // Execute save data
        this.categoryRepository.save(category);
    }

    /**
     *
     * @param categoryRequest
     * @return
     * @throws ProductBusinessException
     */
    @Override
    public CategoryResponse updateCategoryByCode(CategoryRequest categoryRequest) throws ProductBusinessException {

        // Check exist the vendor in the database
        String categoryCode = categoryRequest.getCode();
        Optional<Category> categoryEntity = categoryRepository.findByCode(categoryCode);
        if (categoryEntity.isEmpty()) {
            throw new ProductBusinessException("no data for code: " + categoryCode);
        }

        // Prepare data for update data
        // Prepare data for update category info
        Category categoryData = categoryEntity.get();
        categoryData.setName(!StringUtils.isEmpty(categoryRequest.getName()) ? categoryRequest.getName() : categoryData.getName());
        categoryData.setDescription(!StringUtils.isEmpty(categoryRequest.getDescription()) ? categoryRequest.getDescription() : categoryData.getDescription());

        // Update the category info
        this.categoryRepository.save(categoryData);

        // Get the updated category info and return
        CategoryResponse categoryResponse = new CategoryResponse();
        AppUtils.convertObject(categoryData, categoryResponse);
        return categoryResponse;
    }
}
