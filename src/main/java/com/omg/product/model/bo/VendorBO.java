package com.omg.product.model.bo;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class VendorBO {

    private String vendorCode;

    private String vendorName;
}
