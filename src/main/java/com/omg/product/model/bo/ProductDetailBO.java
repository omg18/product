package com.omg.product.model.bo;

import lombok.*;

import java.math.BigDecimal;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductDetailBO {

    private Long id;

    private String code;

    private String name;

    private String info;

    private String categoryCode;

    private String categoryName;

    private String vendorCode;

    private String vendorName;

    private String brandCode;

    private String brandName;
}
