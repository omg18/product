package com.omg.product.model.request;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@Builder
public class VendorRequest {

    @NotBlank
    private String code;

    @NotBlank
    private String name;

    private String description;

    private String phone;

    private String address;
}
