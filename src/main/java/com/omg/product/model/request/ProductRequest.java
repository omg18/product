package com.omg.product.model.request;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Builder
public class ProductRequest {

    @NotBlank
    private String code;

    @NotBlank
    private String name;

    @NotNull
    private Long categoryId;

    @NotNull
    private Long vendorId;

    @NotNull
    private Long brandId;

    private String info;

}
