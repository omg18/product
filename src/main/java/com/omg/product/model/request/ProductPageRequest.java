package com.omg.product.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 *  @author cuongnh
 *  Product page request
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductPageRequest {

    private Integer pageIndex;

    private Integer pageSize;

    private String categoryCode;

    private String vendorCode;

    private List<String> productCodes;
}
