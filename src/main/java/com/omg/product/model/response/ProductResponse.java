package com.omg.product.model.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductResponse extends BaseResponse{
    private String code;

    private String name;

    private long categoryId;

    private String info;

    private String categoryCode;

    private String categoryName;

    private BigDecimal unitPrice;

}
