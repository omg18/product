package com.omg.product.model.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author hieutt
 * CategoryResponse
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CategoryResponse extends BaseResponse {
    private Long id;

    private String code;

    private String name;

    private String description;
}