package com.omg.product.utils;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author hieutt
 * AppDateUtils
 */
public class DateTimeUtils {

    private DateTimeUtils() {
    }

    /**
     * Convert string to date
     *
     * @param date String
     * @return Date
     * @throws Exception Exception
     */
    public static Date convertStringToDate(String date) {
        if (date == null || date.trim().isEmpty()) {
            return null;
        } else {
            String pattern = "dd/MM/yyyy";
            SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
            dateFormat.setLenient(false);
            try {
                return dateFormat.parse(date);
            } catch (Exception ex) {
                return null;
            }
        }
    }

    /**
     * Convert date to string
     *
     * @param date Date
     * @return String dd/MM/yyyy
     */
    public static String convertDateToString(Date date) {
        if (date == null) {
            return "";
        } else {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            return dateFormat.format(date);
        }
    }

    /**
     * Convert date to string
     *
     * @param date    Date
     * @param pattern String
     * @return String
     */
    public static String convertDateToString(Date date, String pattern) {
        if (date == null) {
            return "";
        } else {
            SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
            return dateFormat.format(date);
        }
    }

    /**
     * Convert local date time to string
     *
     * @param date    LocalDateTime
     * @param pattern String
     * @return String
     */
    public static String convertLocalDateTimeToString(LocalDateTime date, String pattern) {
        if (date == null) {
            return "";
        } else {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
            return formatter.format(date);
        }
    }

    /**
     * convert string to date time
     *
     * @param date    String
     * @param pattern String "dd/MM/yyyy hh:MM:ss"
     * @return Date
     */
    public static Date convertStringToDateTime(String date, String pattern) {
        if (date == null || date.trim().isEmpty()) {
            return null;
        } else {
            SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
            dateFormat.setLenient(false);
            try {
                return dateFormat.parse(date);
            } catch (Exception ex) {
                return null;
            }
        }
    }

    /**
     * Convert string To local date time
     *
     * @param date    String
     * @param pattern String "dd/MM/yyyy hh:MM:ss"
     * @return Date
     */
    public static LocalDateTime convertStringToLocalDateTime(String date, String pattern) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        if (date == null || date.trim().isEmpty()) {
            return null;
        } else {
            try {
                return LocalDateTime.parse(date, formatter);
            } catch (Exception ex) {
                return null;
            }
        }
    }
}
