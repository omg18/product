package com.omg.product.exception;

import com.omg.product.constant.AppConstant;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

/**
 * The ProductExceptionHandler helps we handle all exception type.
 */
@RestControllerAdvice
public class ProductExceptionHandler {

    /**
     * Handle MethodArgumentNotValidException
     * @param ex
     * @return the map contain all error fields and corresponding error messages.
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleInvalidArgument(MethodArgumentNotValidException ex) {

        // This map store the error fields and corresponding error messages
        Map<String, String> errors = new HashMap<String, String>();

        // Get all error fields and corresponding error messages.
        ex.getBindingResult().getFieldErrors().forEach( error -> {

                    // Get info of error field with format: "field name: value"
                    StringBuilder fieldInfo = new StringBuilder();
                    fieldInfo.append(error.getField())
                            .append(AppConstant._COLON)
                            .append(error.getRejectedValue().toString());

                    // Put error field info and error message into the map.
                    errors.put(fieldInfo.toString(), error.getDefaultMessage());
                }
        );

        return errors;
    }

    /**
     * Handle the business exception in Application
     * @param ex
     * @return the info of exception relate the business of application.
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ProductBusinessException.class)
    public Map<String, String> handleBusinessException(ProductBusinessException ex) {
        // This map store the error fields and corresponding error messages
        Map<String, String> errors = new HashMap<String, String>();

        // Add error message to the stored map
        errors.put(AppConstant._LABEL._ERROR_MESSAGE, ex.getMessage());

        return errors;
    }

    /**
     * Handle the other exceptions in Application
     * @param ex
     * @return the info of other exceptions in application.
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public Map<String, String> handleException(Exception ex) {

        // This map store the error fields and corresponding error messages
        Map<String, String> errors = new HashMap<String, String>();

        // Add error message to the stored map
        errors.put(AppConstant._LABEL._ERROR_MESSAGE, ex.getMessage());

        return errors;
    }

}
