package com.omg.product.exception;

/**
 * @author cuongnh
 * The custom exception to handle the business exception
 */
public class ProductBusinessException extends Exception{

    public ProductBusinessException(String errorMessage){
        super(errorMessage);
    }
}
