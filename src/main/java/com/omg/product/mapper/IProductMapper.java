package com.omg.product.mapper;

import com.omg.product.model.bo.ProductDetailBO;
import com.omg.product.model.bo.VendorBO;
import com.omg.product.model.request.ProductPageRequest;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Optional;

/**
 * @author cuongnh
 */
@Mapper
public interface IProductMapper {
    List<ProductDetailBO> queryProductByCodes(ProductPageRequest request);

    ProductDetailBO queryProductDetail(String code);

    Optional<VendorBO> getVendorInfoOfProduct(Long productId);
}
