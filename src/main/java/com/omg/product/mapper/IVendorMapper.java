package com.omg.product.mapper;

import com.omg.product.entity.Vendor;
import org.apache.ibatis.annotations.Mapper;

import java.util.Optional;

/**
 * @author luongtt
 */
@Mapper
public interface IVendorMapper {
    Optional<Vendor> findVendorByCode(String code);
}
