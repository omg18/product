#Build from base image is java 11
FROM openjdk:11

# Get the path of file .jar in local directory
ARG JAR_FILE=target/product*.jar

# Create a working dir in docker
WORKDIR /opt/app

# Set volume point to /tmp
VOLUME /tmp

# Copy file .jar from local to the working dir in docker
# and rename file is product.jar
COPY ${JAR_FILE} product.jar

# define the command default. After container start, these commands are executed.
ENTRYPOINT ["java","-jar","product.jar"]

EXPOSE 10271
