## Overview

- [ ] The module help to manage the product info of system.
- [ ] A product is provided by a vendor and it has the specific category.
- [ ] Also this module help to manage the unit price and budget of the product.

## Setup local

### Step 01: Run mysql in the local with configuration as below:
- [ ] localhost:3306
- [ ] username: root
- [ ] password: 123456
        
Or can run the docker compose file in the repo: SetupLocal/mysql/docker-compose.yml
    
### Step 02: Create new database(schema) "product" in mysql server
### Step 03: Install the redis cache by running the docker compose file in the repo: SetupLocal/redis/docker-compose.yml
### Step 04: Run the module discovery first. And then start the product module.
